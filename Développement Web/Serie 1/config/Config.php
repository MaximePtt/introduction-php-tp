<?php
namespace Configuration;

class Config{
    public static function defaultConfig(){
        $defaultConfig = [
            "namespace" => "Controllers",
            "controller" => "ContactController",
            "method" => "listContacts"
        ];
        return $defaultConfig;
    }

    public static function XMLConfig(){
        $filePath = __DIR__ . "/../web/config.xml";


        $xmlConfig = [
            "namespace" => "Controllers",
            "controller" => "ContactController",
            "method" => "listContacts"
        ];
        return $xmlConfig;
    }
}