<?php
define('ENVIRONMENT', 'development'); // development ou production
if(file_exists(__DIR__."/../vendor/autoload.php")){
    require_once __DIR__."/../vendor/autoload.php";
}
else{
    echo "Fichier inexistant";
}

use Util\Route;

$route = new Route();