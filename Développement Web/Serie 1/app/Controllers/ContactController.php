<?php
namespace Controllers;

use Models\ContactModel;
use Util\View;

class ContactController{
    public function listContacts(){
        $contacts = "";
        $contactModel = new ContactModel();
        $contactsTab = $contactModel->getContacts();
        foreach ($contactsTab as $item) {
            $contacts .= $item."</br>";
        }
        $view = new View();
        $view->render("contacts/index", ["contacts" => $contacts]);
    }

}