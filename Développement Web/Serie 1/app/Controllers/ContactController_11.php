<?php
namespace Controllers;

use Models\ContactModel;
use Util\View;

class ContactController_11
{
    private $view;

    public function __construct()
    {
        $this->view = new View();
         if(ENVIRONMENT == 'development') {
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            $whoops->register();
        }
    }

    public function listContacts()
    {
        $contact = new ContactModel();

        $contacts = $contact->getContacts();

        $this->view->render('contacts/index', ["contacts" => $contacts]);

    }

    public function __toString()
    {
        return "Je suis le controlleur des contacts<br>";

    }
}