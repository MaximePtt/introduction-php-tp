<?php
namespace Models;

use Entity\Personne;
use Exception;
use Faker\Factory;

class ContactModel{
    public function generateContacts(){
        $contacts = [];

        $faker = Factory::create();
        for ($i = 0 ; $i < 10 ; $i++){
            $personne = new Personne($faker->name, $faker->lastName);
            $personne->setEmail($faker->email);
            $personne->setAdresse($faker->address);
            array_push($contacts, $personne);
        }
        $filePath = 'cryptos.json';
        $fp = fopen($filePath, 'w');
        fwrite($fp, json_encode($contacts));
        fclose($fp);
    }

    public function getContacts(){
        $this->generateContacts();
        $contacts = [];

        $filePath = 'cryptos.json';
        $fp = fopen($filePath, 'r');
        $contactsJson = fread($fp, filesize($filePath));
        $personnesObjectsArray = json_decode($contactsJson);
        fclose($fp);

        foreach ($personnesObjectsArray as $object){
            $personne = new Personne($object->nom, $object->prenom);
            if (isset($object->adresse)) $personne->setAdresse($object->adresse);
            if (isset($object->email)) $personne->setEmail($object->email);
            array_push($contacts, $personne);
        }

        return $contacts;
    }
}