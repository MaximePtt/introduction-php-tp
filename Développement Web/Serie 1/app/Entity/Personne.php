<?php
namespace Entity;

use JsonSerializable;

class Personne implements JsonSerializable {
    private $nom;
    private $prenom;
    private $email;
    private $adresse;

    public function __construct($nom, $prenom){
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setEmail(null);
        $this->setAdresse(null);
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getAdresse()
    {
        return $this->adresse;
    }

    public function setAdresse($adresse): void
    {
        $this->adresse = $adresse;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}