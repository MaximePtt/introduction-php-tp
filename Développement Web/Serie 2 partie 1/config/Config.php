<?php
namespace Configuration;

class Config{
    public static function defaultConfig(){
        $defaultConfig = [
            "namespace" => "Controllers",
            "controller" => "CryptoController",
            "method" => "listContacts",
            "jsonFilePath" => "cryptos.json"
        ];
        return $defaultConfig;
    }

    public static function XMLConfig(){
        $filePath = __DIR__ . "/../web/config.xml";


        $xmlConfig = [
            "namespace" => "Controllers",
            "controller" => "CryptoController",
            "method" => "listContacts",
            "jsonFilePath" => "cryptos.json"
        ];
        return $xmlConfig;
    }
}