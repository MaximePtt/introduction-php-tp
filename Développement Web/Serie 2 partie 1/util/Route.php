<?php
namespace Util;

use Configuration\Config;

class Route
{
    public function __construct()
    {
        $configData = Config::defaultConfig();

        $namespace = $configData["namespace"];
        $controller = $configData["controller"];
        $method = $configData["method"];
        $filePath = $configData["jsonFilePath"];

        $url = $_SERVER['REQUEST_URI']; // Recuperation de l'URL
        $url = substr($url, 1); // On enleve le permier "/"
        $parameters = explode("/", $url); // On delimite les elements par des "&"
        $controller = $parameters[1];
        $method = $parameters[2];
        $class = $namespace."\\".$controller;

        if (! class_exists($class)) {
            return $this->not_found();
        }

        if (! method_exists($class, $method)) {
            return $this->not_found();
        }

        $classInstance = new $class;

        // Appel de Controller->listCryptos()

        call_user_func_array(array($classInstance, $method), $args=[$filePath]);
    }

    public function not_found()
    {
        $view = new View();
        $view->render('404');
    }
}