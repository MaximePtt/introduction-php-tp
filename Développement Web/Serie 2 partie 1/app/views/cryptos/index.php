
<?php
if(isset($cryptos)){
    echo '<div class="main-container">';
    foreach ($cryptos as $crypto){
        $nom = $crypto->getNom();
        $image = $crypto->getImage();
        $prix = $crypto->getPrix();

        $imageStyle = 'background-image: url(' . $image . ')';
        echo <<<EOF
            <div class="column-container container">
                <div class="row-container">   
                    <div class="image" style="$imageStyle"></div>
                    <div class="infos-cryptos-container">
                        <div class="nom">$nom</div>
                        <div class="prix">$prix $</div>
                    </div>
                </div>
        EOF;
            echo '<div class="row-container investisseurs">';
            foreach ($crypto->getInvestisseurs() as $investisseur) {
                echo $investisseur . "</br>";
            }
            echo '</div>';
        echo '</div>';
        echo "</br>";
    }
    echo '</div>';
}
