<?php
namespace Entity;
class Crypto{
    private $nom;
    private $image;
    private $prix;
    private $investisseurs;

    public function __construct($nom, $image, $prix, $investisseurs){
        $this->nom = $nom;
        $this->image = $image;
        $this->prix = $prix;
        $this->investisseurs = $investisseurs;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function getPrix()
    {
        return $this->prix;
    }

    public function setPrix($prix): void
    {
        $this->prix = $prix;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image): void
    {
        $this->image = $image;
    }

    public function getInvestisseurs()
    {
        return $this->investisseurs;
    }

    public function setInvestisseurs($investisseurs): void
    {
        $this->investisseurs = $investisseurs;
    }

}