<?php
namespace Controllers;

use Models\CryptoModel;
use Util\View;

class CryptoController{
    private $view;
    private $cryptoModel;

    public function __construct()
    {
        $this->view = new View();
        if(ENVIRONMENT == 'development') {
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            $whoops->register();
        }
    }

    public function listCryptos($filePath){
        $cryptoModel = new CryptoModel();
        $cryptos = $this->cryptoModel->getCryptos($filePath);
        $this->view->render('cryptos/index', ["cryptos" => $cryptos]);

    }

    public function __toString(){
        return "Je suis le controlleur des cryptos<br>";

    }

}