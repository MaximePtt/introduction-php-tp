<?php
namespace Models;

use Entity\Crypto;

class CryptoModel{
    public function getCryptos($filePath){
        $cryptos = [];

        $fp = fopen($filePath, 'r');
        $cryptosJson = fread($fp, filesize($filePath));
        $cryptosObjectsArray = json_decode($cryptosJson);
        fclose($fp);

        foreach ($cryptosObjectsArray as $object){
            array_push($cryptos, new Crypto($object->nom, $object->image, $object->prix, $object->investisseurs));
        }

        return $cryptos;
    }
}