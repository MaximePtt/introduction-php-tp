<?php
define('ENVIRONMENT', 'development'); // development ou production
if(file_exists(__DIR__."/../vendor/autoload.php")){
    require_once __DIR__."/../vendor/autoload.php";
}
else{
    echo "Fichier inexistant";
}

use Configuration\Config;
use Util\Route;
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Application Crypto</title>
    <link href="/assets/styles/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css" integrity="sha512-Fo3rlrZj/k7ujTnHg4CGR2D7kSs0v4LLanw2qksYuRlEzO+tcaEPQogQ0KaoGN26/zrn20ImR1DfuLWnOo7aBA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="nav-bar">
    <div class="title">Liste CryptoMonnaies</div>
</div>
<div class="app">
<?php
$route = new Route();
?>
</div>
</body>
</html>

