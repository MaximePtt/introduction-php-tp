<?php
namespace Models;

use Configuration\Config;
use Entity\Crypto;
use PDO;
use PDOException;

class CryptoModel{
    private $pdo;

    public function __construct(){
        $this->pdo_connect_mysql();
        if(isset($_POST["idCrypto"])){
            $this->updateCryptos($_POST);
        }
    }

    public function pdo_connect_mysql() {
        $dbConfig = Config::dbConfig();
        $host = $dbConfig["host"];
        $user = $dbConfig["user"];
        $pass = $dbConfig["pass"];
        $dbname = $dbConfig["dbname"];
        try {
            $this->pdo = new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8', $user, $pass);
        } catch (PDOException $e) {
            echo "Error !: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getCryptos($filePath){
        $cryptos = [];
        $sql = 'SELECT nom, image, prix, id FROM cryptos';
        $statement = $this->pdo->query($sql);
        $dataCryptos = $statement->fetchAll();

        $investisseurs = [];
        $sql = 'SELECT nom, prenom, fk_cryptos FROM investisseurs';
        $statement = $this->pdo->query($sql);
        $dataInvest = $statement->fetchAll();

        foreach ($dataCryptos as $row) {
            $cryptos[$row["id"]] = new Crypto($row["nom"], $row["image"], $row["prix"], []);
        }
        foreach ($dataInvest as $row) {
            $cryptos[$row["fk_cryptos"]]->addInvestisseur($row["nom"] . " " . $row["prenom"]);
        }

        return $cryptos;
    }

    public function updateCryptos($array){
        $idCrypto = $array["idCrypto"];
        $nomCrypto = $array["nom"];
        $prixCrypto = $array["prix"];
        $investisseurs = [];
        $fin = false;
        $cpt = 0;
        while (!$fin){
            if(isset($array["invest".$cpt])){
                array_push($investisseurs, $array["invest".$cpt]);
                $cpt++;
            }
            else $fin = true;
        }

        $idsInvestisseurs = [];
        $statement = $this->pdo->query("SELECT id FROM investisseurs WHERE fk_cryptos = $idCrypto");
        $dataIdsInvest = $statement->fetchAll();
        foreach ($dataIdsInvest as $row) {
            array_push($idsInvestisseurs, $row["id"]);
        }

        $stmt = $this->pdo->prepare('UPDATE cryptos SET nom = ?, prix = ? WHERE id = ?');
        $stmt->execute([$nomCrypto, $prixCrypto, $idCrypto]);

        foreach ($investisseurs as $key => $investisseur) {
            $investisseurNomPrenom = explode(" ", $investisseur);
            $nom = $investisseurNomPrenom[0];
            $prenom = $investisseurNomPrenom[1];
            $stmt = $this->pdo->prepare('UPDATE investisseurs SET nom = ?, prenom = ? WHERE id = ?');
            $stmt->execute([$nom, $prenom, $idsInvestisseurs[$key]]);
        }
    }
}