
<?php
if(isset($cryptos)){
    echo '<div class="app-container">';
    foreach ($cryptos as $key => $crypto){
        $idCrypto = $key;
        $image = $crypto->getImage();
        $imageStyle = 'background-image: url(' . $image . ')';
        $nom = $crypto->getNom();
        $prix = $crypto->getPrix();
        if(isset($idCryptoEditing) && $idCryptoEditing == $idCrypto) {
            echo <<<EOF
            <div class="column-container main-container">
                <form action="/index.php/CryptoController/listCryptos/0" id="formEditCrypto" method="post">
                    <div class="row-container">   
                        <div class="image" style="$imageStyle"></div>
                        <div class="infos-cryptos-container">
                            <div class="nom"><input class="form-control" type="text" id="nom" name="nom" value="$nom"></div>
                            <div class="prix"><input class="form-control" type="text" id="prix" name="prix" value="$prix">&nbsp;$</div>
                        </div>
                        <div class="buttons-container">
                            <div class="edit-crypto" onClick="document.querySelector('#formEditCrypto').submit(); console.log(querySelector('#formEditCrypto'))"><i class="fas fa-check"></i></div>
                        </div>
                    </div>
        EOF;
                    echo '<div class="column-container investisseurs">';
                        foreach ($crypto->getInvestisseurs() as $keyInvest => $investisseur) {
                            echo '<input class="form-control" type="text" id="invest'.$keyInvest.'" name="invest'.$keyInvest.'" value="'.$investisseur.'">' . "</br>";
                        }
                    echo '</div>';

                echo '</form>';
                    echo '</div>'; // column-container end
                    echo '<input type="hidden" id="idCrypto" name="idCrypto" value="' . $key . '"/>';
        }
        else{
            echo <<<EOF
            <div class="column-container main-container">
                <div class="row-container">   
                    <div class="image" style="$imageStyle"></div>
                    <div class="infos-cryptos-container">
                        <div class="nom">$nom</div>
                        <div class="prix">$prix $</div>
                    </div>
                    <div class="buttons-container">
                        <div class="edit-crypto" onclick="location.href = '$idCrypto';"><i class="fas fa-edit"></i></div>
                    </div>
                </div>
        EOF;
            echo '<div class="row-container investisseurs">';
            foreach ($crypto->getInvestisseurs() as $investisseur) {
                echo $investisseur . "</br>";
            }
            echo '</div>';
            echo '</div>';
            echo "</br>";
        }


    }
    echo '</div>';
}
