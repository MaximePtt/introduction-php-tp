<?php
namespace Controllers;

use Models\CryptoModel;
use Util\View;

class CryptoController{
    private $view;

    public function __construct()
    {
        $this->cryptoModel = new CryptoModel();
        $this->view = new View();
        if(ENVIRONMENT == 'development') {
            $whoops = new \Whoops\Run;
            $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            $whoops->register();
        }
    }

    public function listCryptos($filePath, $option = null){
        $cryptos = $this->cryptoModel->getCryptos($filePath);
        $this->view->render('cryptos/index', ["cryptos" => $cryptos, "idCryptoEditing" => $option]);

    }

    public function __toString(){
        return "Je suis le controlleur des cryptos<br>";

    }

}