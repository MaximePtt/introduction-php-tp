<?php
namespace Configuration;

use PDO;
use PDOException;

class Config{
    public static function defaultConfig(){
        $defaultConfig = [
            "namespace" => "Controllers",
            "controller" => "CryptoController",
            "method" => "listContacts",
            "jsonFilePath" => "cryptos.json"
        ];
        return $defaultConfig;
    }

    public static function XMLConfig(){
        $filePath = __DIR__ . "/../web/config.xml";


        $xmlConfig = [
            "namespace" => "Controllers",
            "controller" => "CryptoController",
            "method" => "listContacts",
            "jsonFilePath" => "cryptos.json"
        ];
        return $xmlConfig;
    }


    public static function dbConfig(){
        $dbConfig = [
            "host" => "mysql.ensinfo.sciences.univ-nantes.prive",
            "user" => "E19C110C",
            "pass" => "JF8N0HCF",
            "dbname" => "E19C110C"
        ];
        return $dbConfig;
    }
}