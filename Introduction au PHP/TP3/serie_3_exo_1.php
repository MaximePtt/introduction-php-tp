<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
<body>
<?php
function reserverPlaces($categoriesCourantes, $categorie, $n){
    $maxCategories = [1 => 25, 2 => 60, 3 => 45];
    if($maxCategories[$categorie] - ($categoriesCourantes[$categorie] + $n) < 0){
        echo "Aucune place disponible</br>";
    }
    elseif($n < 0){
        echo "Réservation négative impossible</br>";
    }
    else{
        $categoriesCourantes[$categorie] += $n;
        echo "Réservation effectuée</br>";
    }

    setCategories($categoriesCourantes);
}

function libererPlaces($categoriesCourantes, $categorie, $n){
    if($categoriesCourantes[$categorie] <= 0){
        echo "Vous ne pouvez pas libérer de place";
    }
    elseif($n < 0){
        echo "Libération négative impossible</br>";
    }
    else{
        $categoriesCourantes[$categorie] -= $n;
    }
    setCategories($categoriesCourantes);
}

function afficherEtatReservations(){
    $categories = getCategories();
    echo <<<EOT
       <table class="table">
          <thead>
            <tr>
              <th scope="col">Catégorie</th>
              <th scope="col">Places Réservées</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>$categories[1] / 25</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>$categories[2] / 60</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>$categories[3] / 45</td>
            </tr>
          </tbody>
        </table>
    EOT;

}

function setCategories($categories){
    $json_categories = json_encode($categories);
    file_put_contents(__DIR__."/categories.json", $json_categories);
}

function getCategories(){
    $json_categories = file_get_contents(__DIR__."/categories.json");
    return json_decode($json_categories, true);
}

function createIfDontExist($filename){
    $file = fopen(__DIR__."/".$filename, 'r+');
    fclose($file);
}

function resetCategories($filepath){
    $categories = [1 => 0, 2 => 0, 3 => 0];
    setCategories($categories);
}



$filepath = __DIR__."/categories.json";

createIfDontExist($filepath);
if(file_get_contents($filepath) == "") {
    resetCategories($filepath);
}
else{
    $categories = getCategories();
}
?>
<div class="container-buttons">
    <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
    <div class="center-div"><b>Réservation de places < Ex 1 < Section 3</b></div>
</div>
<div class="container-with-margin">
    <form method="GET" action="serie_3_exo_1.php">
        <div class="form-group">
            <label for="categorie">Catégorie</label>
            <select name="categorie" class="custom-select">
                <option value="" selected>--Choisissez une catégorie--</option>
                <option value="1">Catégorie 1</option>
                <option value="2">Catégorie 2</option>
                <option value="3">Catégorie 3</option>
            </select>
        </div>
        <div class="form-group">
            <label for="nombrePlaces">Nombre de places</label>
            <input type="number" class="form-control" name="nombrePlaces">
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="isReservation" id="reservation" value="true" checked>
            <label class="form-check-label" for="reservation">
                Réserver
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="isReservation" id="liberation" value="false">
            <label class="form-check-label" for="liberation">
                Libérer
            </label>
        </div>
        </br>
        <input type="hidden" name="loaded_1" value="true">
        <button type="submit" class="btn btn-primary">Valider</button>
    </form>
</div>
<div class="container-with-margin">
    <?php
        if(isset($_GET["loaded_1"]) && $_GET["categorie"] != ""){
            if($_GET["isReservation"] == "true"){
                reserverPlaces($categories, $_GET["categorie"], $_GET["nombrePlaces"]);
            }
            else{
                libererPlaces($categories, $_GET["categorie"], $_GET["nombrePlaces"]);
            }
        }
    ?>
</div>
<div class="container-with-margin">
    <form method="GET" action="serie_3_exo_1.php">
        <input type="hidden" name="loaded_2" value="true">
        <button type="submit" class="btn btn-primary">Afficher l'état des réservation</button>
    </form>
</div>
<div class="container-with-margin">
    <?php
    if(isset($_GET["loaded_2"])){
        afficherEtatReservations();
    }
    ?>
</div>
</body>
</html>
