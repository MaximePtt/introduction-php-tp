<?php
    session_start();
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Jeu de jetons</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
<body>
<div class="container-buttons">
    <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
    <div class="center-div"><b>Jeu de jetons < Ex 3 < Section 3</b></div>
</div>
<div class="container-with-margin">
    <form>
        <div class="form-group">
            <label for="jetons">Nombre de jetons à retirer</label>
            <select class="custom-select" name="jetons">
                <option value="1" selected>1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
        <input type="hidden" name="loaded" value="true">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<div class="container-with-margin">
    <?php
    if(!isset($_SESSION["joueur"])){
        $_SESSION["joueur"] = rand(1, 2);
        $_SESSION["finished"] = false;
        $_SESSION["jetons"] = rand(10, 100);

        echo "Joueur " . $_SESSION["joueur"] . " commence</br>";
        echo "Il y a " . $_SESSION["jetons"] . " jetons</br>";
    }
    else {
        $_SESSION["jetons"] -= $_GET["jetons"]; // On soustrait le nombre de jetons à retirer au total des jetons
        if($_SESSION["jetons"] < 0) $_SESSION["jetons"] = 0; // Pas de nombre de jetons négatif
        if($_SESSION["jetons"] == 0){
            $_SESSION["finished"] = true;
        }
        else{
            echo "C'est au tour du joueur " . $_SESSION["joueur"] . "</br>";
            echo "Il y reste " . $_SESSION["jetons"] . " jetons</br>";
        }
        $_SESSION["joueur"] = ($_SESSION["joueur"] == 1 ? 2 : 1); // Changement de joueur à chaque tour
    }
    echo $_SESSION["jetons"] . " est divisible par 7 : " . ($_SESSION["jetons"] % 7 == 0 ? "Oui" : "Non") . "</br>";
    if($_SESSION["finished"]){
        echo "Le joueur " . $_SESSION["joueur"] . " a gagné !</br>";
        session_unset();
    }
    ?>
</div>
</body>
</html>
