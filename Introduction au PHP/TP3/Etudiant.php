<?php
class Etudiant
{
    private $num;
    private $nom;
    private $prenom;
    public function __construct($num, $nom, $prenom){
        $this->num = $num;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public function getNum()
    {
        return $this->num;
    }

    public function setNum($num): void
    {
        $this->num = $num;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom()
    {
        return $this->prenom;
    }

    public function setPrenom($prenom): void
    {
        $this->prenom = $prenom;
    }
}