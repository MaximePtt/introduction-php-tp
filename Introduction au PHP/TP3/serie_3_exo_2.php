<?php
include_once 'Etudiant.php';
?>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
<body>
<?php
function afficherEtudiants($etudiants){
    echo '<table class="table">';
    echo '<thead>';
    echo '<tr>';
    echo '<th scope="col">Numéro</th>';
    echo '<th scope="col">Nom</th>';
    echo '<th scope="col">Prénom</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    foreach ($etudiants as $key => $etudiant){
        echo "<tr>";
        echo "<td>".$etudiant->getNum()."</td>";
        echo "<td>".$etudiant->getNom()."</td>";
        echo "<td>".$etudiant->getPrenom()."</td>";
        echo "</tr>";
    }
    echo '</tbody>';
    echo '</table>';
}

function setEtudiants($etudiants){
    $json_etudiants = createJsonEtudiants($etudiants);
    file_put_contents(__DIR__."/etudiants.json", $json_etudiants);
}

function getEtudiants(){
    $json_etudiants = file_get_contents(__DIR__."/etudiants.json");
    return json_decode($json_etudiants, true);
}

function createIfDontExist($filename){
    $file = fopen(__DIR__."/".$filename, 'r+');
    fclose($file);
}

function resetEtudiants(){
    $etudiants = [];
    setEtudiants($etudiants);
}

function createJsonEtudiant($etudiant){
    return "{".'"num" : "'.$etudiant->getNum().'", "nom" : "'.$etudiant->getNom().'", "prenom" : "'.$etudiant->getPrenom().'"}';
}

function createJsonEtudiants($etudiants){

    $jsonEtudiants = "[";

    for ($i = 0 ; $i < count($etudiants) ; $i++){
        $jsonEtudiants .= createJsonEtudiant($etudiants[$i]) . ($i == count($etudiants) - 1 ? "" : ", ");
    }
    $jsonEtudiants .= "]";
    return $jsonEtudiants;
}



$filepath = __DIR__."/etudiants.json";

createIfDontExist($filepath);
if(file_get_contents($filepath) == "") {
    resetEtudiants();
}
else{
    $etudiants = getEtudiants();
}
?>
<div class="container-buttons">
    <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
    <div class="center-div"><b>Afficher instances de la classe Etudiant < Ex 1 < Section 3</b></div>
</div>
<div class="container-with-margin">
    <?php
        array_push($etudiants, new Etudiant("E19C110C", "Potet", "Maxime"));
        array_push($etudiants, new Etudiant("E216583R", "Vatin", "Alexis"));
        setEtudiants($etudiants);
        afficherEtudiants($etudiants);
    ?>
</div>
</body>
</html>
