<?php
include "config.php";

$pdo = pdo_connect_mysql();

// Est-ce que l'id existe et n'est pas vide dans l'URL
if (isset($_GET['id'])) {

    $stmt = $pdo->prepare('SELECT * FROM users WHERE id = ?');
    $stmt->execute([$_GET['id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$user) {
        exit('L\'utilisateur avec cet ID n\'existe pas');
    }

    // On vérifie si l'utilisateur existe
    if (!$user) {
        $_SESSION['erreur'] = "Cet id n'existe pas";
    }
} else {
    $_SESSION['erreur'] = "URL invalide";
}
?>

<?= template_header('Détails utilisateur') ?>

    <div class="content update mt-3 container-with-margin">
        <h1><?= $user['Prenom'] . ' ' . $user['Nom'] ?></h1>
        <p>Email : <?= $user['Email'] ?></p>
        <p>Téléphone : <?= $user['Contact'] ?></p>
        <p>Adresse : <?= $user['Adresse'] ?></p>
        <a href="update.php?id=<?= $user['id'] ?>">
            <button class="btn btn-primary">Modifier</button>
        </a>
        <a href="index.php">
            <button class="btn btn-danger">Retour</button>
        </a>
    </div>
<?= template_footer() ?>