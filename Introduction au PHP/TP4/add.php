<?php

include 'config.php';
$pdo = pdo_connect_mysql();
$msg = '';
if (!empty($_POST)) {
    $prenom = filter_input(INPUT_POST, 'prenom');
    $nom = filter_input(INPUT_POST, 'nom');
    $email = filter_input(INPUT_POST, 'email');
    $contact = filter_input(INPUT_POST, 'contact');
    $adresse = filter_input(INPUT_POST, 'adresse');
    $stmt = $pdo->prepare('INSERT INTO users (prenom, nom, email, contact, adresse)
                                            VALUES(:prenom, :nom, :email, :contact, :adresse)');
    if (false === $stmt) {
        throw new Exception('Invalid prepare statement');
    }
    if (false === $stmt->execute([':prenom' => $prenom, ':nom' => $nom, ':email' => $email, ':contact' => $contact, ':adresse' => $adresse])) {
        throw new Exception(implode('', $stmt->errorInfo()));
    }

    // soit on affiche le message de réussite, soit on redirige vers l'ecran d'accueil
    //$msg = 'Creation réussie!';
    header('Location: index.php');
}
?>

<?= template_header('Create') ?>

<div class="content update m-5">
    <h1>Ajouter un utilisateur</h1>
    <br><br>
    <form action="add.php" method="post">
        <div class="form-row">
            <div class="for-group col-md-6">
                <label for="nom">Nom</label>
                <input type="text" class="form-control" name="nom" id="nom">
            </div>
            <div class="for-group col-md-6">
                <label for="prenom">Prenom</label>
                <input type="text" class="form-control" name="prenom" id="prenom">
            </div>
        </div>
        <div class="form-row">
            <div class="for-group col-md-6">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" id="email">
            </div>
            <div class="for-group col-md-6">
                <label for="contact">Portable</label>
                <input type="text" class="form-control" name="contact" id="contact">
            </div>
        </div>
        <div>
            <div class="for-group">
                <label for="adresse">Adresse</label>
                <textarea class="form-control" id="adresse" name="adresse" rows="3"></textarea>
            </div>
        </div>
        <div class="mt-3">
            <button type="submit" class="btn btn-success">Ajouter</button>
            <a href="index.php">
                <div class="btn btn-danger">Annuler</div>
            </a>
        </div>
    </form>



    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
</div>

<?= template_footer() ?>
