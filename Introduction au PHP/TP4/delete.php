<?php
include 'config.php';
$pdo = pdo_connect_mysql();
$msg = '';
// verifier si ID existe
if (isset($_GET['id'])) {
    $stmt = $pdo->prepare('SELECT * FROM users WHERE id = ?');
    $stmt->execute([$_GET['id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$user) {
        exit('L\'utilisateur avec cet ID n\'existe pas');
    }
    // Message de confirmation
    if (isset($_GET['confirm'])) {
        if ($_GET['confirm'] == 'yes') {
            // Si on clique oui
            $stmt = $pdo->prepare('DELETE FROM users WHERE id = ?');
            $stmt->execute([$_GET['id']]);
            $msg = 'L\'utilisateur à été supprimé';
        } else {
            // Si on clique non
            header('Location: index.php');
            exit;
        }
    }
} else {
    exit('Aucun ID spécifié');
}
?>
<?=template_header('Supprimer utilisateur')?>

<div class="content delete container-with-margin">
    <h2>Supprimer l'utilisateur #<?=$user['id']?></h2>
    <?php
    if ($msg) {
        echo '<p><?=$msg?></p>';
        header('Location: index.php');
     }else{ ?>
        <p>Etes vous sur de vouloir supprimer <?=$user['Prenom'] . " " .  $user['Nom']?> ?</p>
        <div class="choice">
            <a class="btn btn-danger" href="delete.php?id=<?=$user['id']?>&confirm=yes">Oui</a>
            <a class="btn btn-success"href="delete.php?id=<?=$user['id']?>&confirm=no">Non</a>
        </div>
    <?php } ?>
</div>

<?=template_footer()?>
