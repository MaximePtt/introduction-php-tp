<?php
include 'config.php';
$pdo = pdo_connect_mysql();
$msg = '';
// Vérifier si le contact existe
if (isset($_GET['id'])) {
    if (!empty($_POST)) {
        // This part is similar to the create.php, but instead we update a record and not insert
        $prenom = filter_input(INPUT_POST, 'prenom');
        $nom = filter_input(INPUT_POST, 'nom');
        $email = filter_input(INPUT_POST, 'email');
        $contact = filter_input(INPUT_POST, 'contact');
        $adresse = filter_input(INPUT_POST, 'adresse');
        // Update the record
        $stmt = $pdo->prepare('UPDATE users SET Prenom = ?, Nom = ?, email = ?, Contact = ?, Adresse = ? WHERE id = ?');
        $stmt->execute([$prenom, $nom, $email, $contact, $adresse, $_GET['id']]);
        $msg = 'Utilisateur mis à jour';
    }
    // Get the contact from the contacts table
    $stmt = $pdo->prepare('SELECT * FROM users WHERE id = ?');
    $stmt->execute([$_GET['id']]);
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    if (!$user) {
        exit('L\'utilisateur n\'existe pas');
    }
} else {
    exit('Aucun ID spécifié');
}
if (!empty($_POST)) {
    header('Location: index.php');

}
?>

<?= template_header('Update') ?>

<div class="content update m-5">
    <h2>Mise à jour de : <?= $user['Prenom'] . " " . $user['Nom'] ?></h2>
    <form action="update.php?id=<?= $user['id'] ?>" method="post">
        <div class="form-row">
            <div class="for-group col-md-6">
                <label for="nom">Nom</label>
                <input type="text" name="nom" value="<?= $user['Nom'] ?>" id="nom" class="form-control">
            </div>
            <div class="for-group col-md-6">
                <label for="prenom">Prenom</label>
                <input type="text" name="prenom" value="<?= $user['Prenom'] ?>" id="prenom" class="form-control">
            </div>
        </div>
        <div class="form-row">
            <div class="for-group col-md-6">
                <label for="email">Email</label>
                <input type="email" name="email" value="<?= $user['Email'] ?>" id="email" class="form-control">
            </div>
            <div class="for-group col-md-6">
                <label for="contact">Portable</label>
                <input type="text" name="contact" value="<?= $user['Contact'] ?>" id="contact" class="form-control">
            </div>
        </div>
        <div>
            <div class="for-group">
                <label for="adresse">Adresse</label>
                <textarea name="adresse" id="adresse" class="form-control"><?= $user['Adresse'] ?></textarea>
            </div>
        </div>
        <div class="mt-3">
            <button type="submit" class="btn btn-success">Editer</button>
            <a href="index.php">
                <div class="btn btn-danger">Annuler</div>
            </a>
        </div>
    </form>

    <?php if ($msg): ?>
        <p><?= $msg ?></p>
    <?php endif; ?>
</div>

<?= template_footer() ?>
