<?php
include 'config.php';

/* L'incude config.php donne accèes à tout le fichier et ce qu'il y a dedans
Meme si les variables sont soulignées comme une erreur, elles sont reconnus
*/
?>

<?= template_header('Gestion des utilisateurs') // Home Page template below. ?>

    <div class="content read">
        <div class="container-with-margin">
            <h1>Gestion des utilisateurs</h1>
        </div>
        <div class="container-with-margin">
            <button onclick="location.href = 'add.php'" class="btn btn-success">Ajouter un utilisateur</button>
            </br></br>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Contact</th>
                    <th scope="col">Adresse</th>
                    <th scope="col">Date inscription</th>
                    <th scope="col">Editer</th>
                    <th scope="col">Supprimer</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr onclick="location.href = 'read.php?id=<?= $user['id'] ?>'" class="clickable">
                        <td><?= $user['id'] ?></td>
                        <td><?= $user['Nom'] ?></td>
                        <td><?= $user['Prenom'] ?></td>
                        <td><?= $user['Email'] ?></td>
                        <td><?= $user['Contact'] ?></td>
                        <td><?= $user['Adresse'] ?></td>
                        <td><?= $user['InscriptionDate'] ?></td>

                        <td>
                            <div class="center-div">
                                <a href="update.php?id=<?= $user['id'] ?>'">
                                    <button class="btn btn-primary"><i class="fas fa-pencil-alt"></i></button>
                                </a>
                            </div>
                        </td>
                        <td>
                            <div class="center-div">
                                <a href="delete.php?id=<?= $user['id'] ?>">
                                   <button class="btn btn-danger"><i class="fas fa-trash-alt"></i></button>
                                </a>
                            </div>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="pagination">
                <?php if ($page > 1): ?>
                    <a href="index.php?page=<?= $page - 1 ?>"><i class="fas fa-angle-double-left fa-sm"></i></a>
                <?php endif; ?>
                <?php if ($page * $records_per_page < $num_users): ?>
                    <a href="index.php?page=<?= $page + 1 ?>"><i class="fas fa-angle-double-right fa-sm"></i></a>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?= template_footer() ?>