<?php
function pdo_connect_mysql() {
    $host = 'localhost';
    $user = 'root';
    $pass = '';
    $dbname = 'php';
    try {
        return new PDO('mysql:host=' . $host . ';dbname=' . $dbname . ';charset=utf8', $user, $pass);
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
}
function template_header($title) {
    echo <<<EOT
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>$title</title>
	    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link href="../styleold.css" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
	</head>
	<body>
    <div class="container-buttons">
        <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
        <div class="center-div"><b>CRUD php < Section 4</b></div>
    </div>
EOT;
}
function template_footer() {
    echo <<<EOT
    </body>
</html>
EOT;
}

// Your PHP code here.
// Connexion
$pdo = pdo_connect_mysql();
// Verifier si la page existe, sinon renvoyer 1
$page = isset($_GET['page']) && is_numeric($_GET['page']) ? (int)$_GET['page'] : 1;
// nombre d'enregistrements a afficher
$records_per_page = 5;

// Requete sql, prepare + bindvalue + execute
$query = $pdo->prepare('SELECT * FROM users ');
$query->execute();
// Fetch de la requete
$users = $query->fetchAll(PDO::FETCH_ASSOC);
$num_users = $pdo->query('SELECT COUNT(*) FROM users')->fetchColumn();
?>