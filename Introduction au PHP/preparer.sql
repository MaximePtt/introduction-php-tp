-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : Dim 09 jan. 2022 à 11:21
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21
-- Vatin Alexis, Potet Maxime

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `php`
--
CREATE DATABASE IF NOT EXISTS `php` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `php`;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Prenom` varchar(100) NOT NULL,
  `Nom` varchar(100) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Contact` char(11) NOT NULL,
  `Adresse` varchar(255) NOT NULL,
  `InscriptionDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `Prenom`, `Nom`, `Email`, `Contact`, `Adresse`, `InscriptionDate`) VALUES
(1, 'Damien', 'Bourdon', 'bourdon-d@gmail.com', '0657128545', 'Route de la Jonelière, 44300 Nantes', '2018-11-28 04:41:26'),
(2, 'Marie', 'Dupont', 'dupont-m@yahoo.fr', '0654789823', 'Avenue de la république, 96032 Pradines', '2022-01-07 15:40:53'),
(3, 'Alexis', 'Vatin', 'alexis.vatin1@gmail.com', '0652662715', '2 avenue des franciscains, batiment 2', '2022-01-08 12:16:14'),
(4, 'Maxime', 'Potet', 'potet-m@gmail.com', '0695271532', '7 rue du roti ', '2022-01-08 13:18:38'),
(5, 'Fredon', 'Sacquet', 'sacquet-f@fondcombe.fr', '', '1 boulevard du mordor, Terre du millieu ', '2022-01-08 13:19:30');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
