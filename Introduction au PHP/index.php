<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="./style.css" rel="stylesheet">
</head>
<body>
    <div class="container-with-margin">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Série 1</th>
                    <th scope="col">Ex1</th>
                    <th scope="col">Ex2</th>
                    <th scope="col">Ex3</th>
                    <th scope="col">Ex4</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td><a href="TP1/serie_1_exo_1.php">Lien</a></td>
                    <td><a href="TP1/serie_1_exo_2.php">Lien</a></td>
                    <td><a href="TP1/serie_1_exo_4.php">Lien</a></td>
                    <td><a href="TP1/serie_1_exo_3.php">Lien</a></td>
                </tr>
                <tr>
                    <th scope="row">2</th><td>
                        <a href="TP2/serie_2_exo_1.php">Lien</a></td>
                    <td><a href="TP2/serie_2_exo_2.php">Lien</a></td>
                    <td><a href="TP2/serie_2_exo_3.php">Lien</a></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td><a href="TP3/serie_3_exo_1.php">Lien</a></td>
                    <td><a href="TP3/serie_3_exo_2.php">Lien</a></td>
                    <td><a href="TP3/serie_3_exo_3.php">Lien</a></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td><a href="TP4/">Lien</a></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</body>
</html>