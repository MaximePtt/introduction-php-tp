# Projet php, Vatin Alexis - Potet Maxime
## Voici une petit fichier qui présente l'arborescence du projet ainsi que les démarches à suivre pour la mise en place de la base de données

## Arborescence : 
Projet_Vatin_Potet  
* TP1  
  * ex.1php
  * ex.2php
  * ex.3php
  * ex.4php

* TP2    
    * ex1.php
    * ex2.php
    * ex3.php
    * ex4.php
    * ex4/    
      * Camion.php  
      * Deux_roues.php  
      * Quatre_roues.php  
      * Vehicule.php  
      * Voiture.php
    
* TP3  
    * ex.1php
    * ex.2php
    * ex.3php


* TP4
    * add.php
    * config.php
    * delete.php
    * index.php
    * read.php
    * style.css
    * update.php  
     
Le fichier "config.php" contient tout ce qui est nécessaire
pour la connexion à la base de données, ainsi que des fonctions
de création d'hearder et footer pour limiter les répétitions
 
* index.php
* README.md  
* style.css  
* preparer.sql

## Concernant le base de données :  
Le fichier preparer.sql contient de quoi la base de données nommée 
"php" ainsi  la table "users" et inserer des données dans celle ci (5 tuples). 

     
  