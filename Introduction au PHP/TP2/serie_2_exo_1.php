<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Cryptage méthode César</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
    <body>
    <?php
        function crypter($text, $decallage){
            $length = strlen($text);
            $alphabet = "abcdefghijklmnopqrstuvwxyz";
            $crypte = "";
            for($i = 0 ; $i < $length ; $i++){
                $positionCharAlphabet = strpos($alphabet, $text[$i]);
                $newPositionCharAlphabet = ($positionCharAlphabet + $decallage) % 26;
                $crypte[$i] = $alphabet[$newPositionCharAlphabet];
            }
            return $crypte;
        }

        function decrypter($text, $decallage){
            $length = strlen($text);
            $alphabet = "abcdefghijklmnopqrstuvwxyz";
            $decrypte = "";
            for($i = 0 ; $i < $length ; $i++){
                $positionCharAlphabet = strpos($alphabet, $text[$i]);
                $newPositionCharAlphabet = ($positionCharAlphabet - $decallage) % 26;
                $decrypte[$i] = $alphabet[$newPositionCharAlphabet];
            }
            return $decrypte;
        }
    ?>
        <div class="container-buttons">
            <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
            <div class="center-div"><b>Cryptage méthode César < Ex 1 < Section 2</b></div>
        </div>
        <div class="container-with-margin" id="crypter">
            <form method="GET" action="serie_2_exo_2.php">
                <div class="form-group">
                    <label for="aCrypter">Crypter</label>
                    <input type="text" class="form-control" name="aCrypter" placeholder="Texte à crypter">
                </div>
                <div class="form-group">
                    <label for="decallageCryptage">Décallage</label>
                    <input type="number" class="form-control" name="decallageCryptage" placeholder="Décallage">
                </div>
                <input type="hidden" name="loaded_1" value="true">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <?php
            $aCrypter = $_GET["aCrypter"];
            $decallageCryptage = $_GET["decallageCryptage"];
            if(($aCrypter == "" || $decallageCryptage == "") && $_GET["loaded"] == "true"){
                echo "Données incomplètes";
            }
            elseif(isset($aCrypter, $decallageCryptage)){
                $crypte = crypter($aCrypter, $decallageCryptage);
                echo 'En cryptant "' . $aCrypter . '" avec un décallage de ' . $decallageCryptage . ', nous obtenons "' . $crypte . '".';
            }
            ?>
        </div>
        <div class="container-with-margin" id="decrypter">
            <form method="GET" action="serie_2_exo_2.php">
                <div class="form-group">
                    <label for="aDecrypter">Décrypter</label>
                    <input type="text" class="form-control" name="aDecrypter" placeholder="Texte à décrypter">
                </div>
                <div class="form-group">
                    <label for="decallageDecryptage">Décallage</label>
                    <input type="number" class="form-control" name="decallageDecryptage" placeholder="Décallage">
                </div>
                <input type="hidden" name="loaded_2" value="true">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <?php
            $aDecrypter = $_GET["aDecrypter"];
            $decallageDecryptage = $_GET["decallageDecryptage"];
            if(($aDecrypter == "" || $decallageDecryptage == "") && $_GET["loaded"] == "true"){
                echo "Données incomplètes";
            }
            elseif(isset($aDecrypter, $decallageDecryptage)){
                $decrypte = decrypter($aDecrypter, $decallageDecryptage);
                echo 'En décryptant "' . $aDecrypter . '" avec un décallage de ' . $decallageDecryptage . ', nous obtenons "' . $decrypte . '".';
            }
            ?>
        </div>
    </body>
</html>
