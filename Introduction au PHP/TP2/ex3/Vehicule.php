<?php
class Vehicule{
    private $couleur;
    private $poids;

    public function __construct($couleur, $poids){
        $this->couleur = $couleur;
        $this->poids = $poids;
    }

    /* METHODS */

    public function rouler(){
        echo "Je roule";
    }

    public function ajouterPersonne($poids){
        $this->setPoids($this->getPoids() + $poids);
    }

    public function getCouleur(){
        return $this->couleur;
    }

    public function getPoids(){
        return $this->poids;
    }

    public function setPoids($poids){
        $this->poids = $poids;
    }

    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    }
}