<?php
class Camion extends Quatre_roues
{
    private $longueur;

    /* METHODS */

    public function ajouter_remorque($longueur_remorque){
        $this->setLongueur($this->getLongueur() + $longueur_remorque);
    }

    /* GETTERS */

    public function getLongueur()
    {
        return $this->longueur;
    }

    /* SETTERS */

    public function setLongueur($longueur): void
    {
        $this->longueur = $longueur;
    }


}