<?php
class Voiture extends Quatre_roues
{
    private $nombre_pneu_neige;

    public function __construct($couleur, $poids){
        parent::__construct($couleur, $poids);
    }

    /* METHODS */

    public function ajouter_pneu_neige($nombre){
        if($this->nombre_pneu_neige + $nombre > 4) $this->setNombrePneuNeige(4);
        else $this->setNombrePneuNeige($this->nombre_pneu_neige + $nombre);
    }

    public function enlever_pneu_neige($nombre){
        if($this->nombre_pneu_neige - $nombre < 0) $this->setNombrePneuNeige(0);
        else $this->setNombrePneuNeige($this->nombre_pneu_neige - $nombre);
    }

    /* GETTERS */

    public function getNombrePneuNeige()
    {
        return $this->nombre_pneu_neige;
    }

    /* SETTERS */

    public function setNombrePneuNeige($nombre_pneu_neige): void
    {
        $this->nombre_pneu_neige = $nombre_pneu_neige;
    }
}