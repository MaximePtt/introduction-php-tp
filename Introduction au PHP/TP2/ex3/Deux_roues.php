<?php
class Deux_roues extends Vehicule
{
    private $cylindree;

    /* METHODS */

    public function mettre_essence($nombre_litre){
        parent::setPoids($nombre_litre * 0.9);
    }

    /* GETTERS */

    public function getCylindree()
    {
        return $this->cylindree;
    }

    /* SETTERS */

    public function setCylindree($cylindree): void
    {
        $this->cylindree = $cylindree;
    }


}