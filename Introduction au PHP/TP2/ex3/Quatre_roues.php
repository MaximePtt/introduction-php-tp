<?php
class Quatre_roues extends Vehicule
{
    private $nombre_porte;

    public function __construct($couleur, $poids){
        parent::__construct($couleur, $poids);
    }

    /* METHODS */

    public function repeindre($couleur){
        $this->setCouleur($couleur);
    }

    /* GETTERS */

    public function getNombrePorte()
    {
        return $this->nombre_porte;
    }

    /* SETTERS */

    public function setNombrePorte($nombre_porte): void
    {
        $this->nombre_porte = $nombre_porte;
    }


}