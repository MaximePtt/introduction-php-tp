<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
    <body>
        <?php
            function est_pair($entier){
                return $entier % 2 == 0;
            }

            function moitie($entier){
                return $entier / 2;
            }

            function un_plus_triple($entier){
                return 3 * $entier + 1;
            }

            function saisie_entier_positif($number){
                return abs(round($number));
            }

            function increment_enigme($number){
                $entier = saisie_entier_positif($number);
                if(est_pair($entier)) $entier = moitie($entier);
                else $entier = un_plus_triple($entier);
                return $entier;
            }

            function get_table_enigme($nbDepart, $nbIterations){
                $tab = [];
                $nbCourant = $nbDepart;
                array_push($tab, $nbCourant);
                for($i = 0 ; $i < $nbIterations ; $i++){
                    $nbCourant = increment_enigme($nbCourant);
                    array_push($tab, $nbCourant);
                }
                return $tab;
            }

            function print_vector($tab, $nb_col){
                echo '<table class="table">';
                echo '<thead>
                            <tr>
                              <th scope="col" colspan="'. $nb_col .'">
                                <div class="centered-text">
                                Suite des nombres obtenus
                                </div>
                              </th>
                            </tr>
                         </thead>';
                echo '<tbody>';
                for($i = 0 ; $i < count($tab) ; $i++){
                    if($i % $nb_col == 0 || $i == 0) echo '<tr>';
                    echo '<td title="' . $i . '" class="' . ($i == 0 ? 'red-bold-text' : '') . '">' . $tab[$i] . '</td>';
                    if($i % $nb_col == $nb_col - 1) echo '</tr>';
                }
                echo '</tbody>';
                echo '</table>';
            }
        ?>
        <div class="container-buttons">
            <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
            <div class="center-div"><b>Enigme mathématique < Ex 2 < Section 2</b></div>
        </div>
        <div class="container-with-margin">
            <form method="GET" action="serie_2_exo_2.php">
                <div class="form-group">
                    <label for="nombreDepart">Nombre de départ</label>
                    <input type="number" class="form-control" name="nombreDepart">
                </div>
                <div class="form-group">
                    <label for="nombreIterations">Nombre d'itérations</label>
                    <input type="number" class="form-control" name="nombreIterations">
                </div>
                <input type="hidden" name="loaded" value="true">
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="container-with-margin">
            <?php
                if(isset($_GET["nombreDepart"], $_GET["nombreIterations"], $_GET["loaded"])) {
                    $tab = get_table_enigme($_GET["nombreDepart"], $_GET["nombreIterations"]);
                    if (count($tab) > 0) {
                        print_vector($tab, 20);
                    }
                }
            ?>
        </div>
    </body>
</html>
