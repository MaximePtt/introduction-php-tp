<?php
    include 'ex3/Vehicule.php';
    include 'ex3/Quatre_roues.php';
    include 'ex3/Voiture.php';
    include 'ex3/Camion.php';
    include 'ex3/Deux_roues.php';
    session_start();
?>
<?php
function afficheVoiture(){
    $couleurs = ["red" => "#a33131", "green" => "#5DA331", "blue" => "#2955A3"];
    if(!isset($_SESSION["voiture"])) $_SESSION["voiture"] = new Voiture("green", "1400");
    if(isset($_GET["loaded_1"])) $_SESSION["voiture"]->setCouleur($_GET["repeindre_v"]);
    if(isset($_GET["loaded_2"])) $_SESSION["voiture"]->ajouterPersonne($_GET["personne_v"]);
    if(isset($_GET["loaded_3"])){
        $_SESSION["voiture"] = new Voiture($_GET["couleur_v"], $_GET["poids_v"]);
        for($i = 0 ; $i < $_GET["nb_personne_v"] ; $i++){
            $_SESSION["voiture"]->ajouterPersonne($_GET["personne_v"]);
        }
    }
    $couleur = $_SESSION["voiture"]->getCouleur();
    echo <<<EOT
            <style>
                .carrosserie-voiture {
                    background-color: $couleurs[$couleur];
                }
            </style>
         EOT;

    if(isset($_GET["rouler_v"]) && $_GET["rouler_v"] == "true"){
        $roule_class = "roule";
        $roule_text = "La voiture roule";
    }
    else{
        $roule_class = "";
        $roule_text = "La voiture est à l'arrêt";
    }
    $couleur_voiture = $_SESSION["voiture"]->getCouleur();
    $poids_voiture = $_SESSION["voiture"]->getPoids();


    echo <<<EOT
        <div class="voiture_container">
            <div class="voiture $roule_class">
                <div class="case case_vide"></div>
                <div class="case carrosserie-voiture"></div>
                <div class="case carrosserie-voiture">
                    <div class="fenetre"></div>
                </div>
                <div class="case case_vide"></div>
                <div class="case carrosserie-voiture"></div>
                <div class="case carrosserie-voiture"></div>
                <div class="case carrosserie-voiture"></div>
                <div class="case carrosserie-voiture capot"></div>
                <div class="roue">
                    <div class="jante"></div>
                </div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="roue">
                    <div class="jante"></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div>Couleur : $couleur_voiture</div>
            <div>Poids : $poids_voiture kg</div>
            <div>$roule_text</div>
        </div>
        EOT;
}

function afficheCamion(){
    $couleurs = ["red" => "#a33131", "green" => "#5DA331", "blue" => "#2955A3"];
    if(!isset($_SESSION["camion"]) || isset($_GET["reset_c"])){
        $_SESSION["camion"] = new Camion("green", "1400");
        $_SESSION["camion"]->setNombrePorte(2);
        $_SESSION["camion"]->setLongueur(10);

    }
    if(isset($_GET["loaded_4"])){
        $_SESSION["camion"] = new Camion("red", $_GET["poids_c"]);
        $_SESSION["camion"]->setLongueur($_GET["longueur_c"]);
        $_SESSION["camion"]->setNombrePorte($_GET["portes_c"]);
        $_SESSION["camion"]->ajouter_remorque($_GET["remorque_c"]);
    }
    $couleur = $_SESSION["camion"]->getCouleur();
    echo <<<EOT
            <style>
                .carrosserie-camion {
                    background-color: $couleurs[$couleur];
                }
            </style>
         EOT;
    $longueur_camion = $_SESSION["camion"]->getLongueur();
    $portes_camion = $_SESSION["camion"]->getNombrePorte();
    $poids_camion = $_SESSION["camion"]->getPoids();


    echo <<<EOT
        <div class="camion_container">
            <div class="camion">
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie_grise">
                    <div class="fenetre"></div>
                </div>
                
                <div class="case case_vide"></div>
                
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie-camion"></div>
                <div class="case carrosserie_grise"></div>
                <div class="case carrosserie_grise capot"></div>
                
                
                <div class="roue">
                    <div class="jante"></div>
                </div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="roue">
                    <div class="jante"></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div>Longueur : $longueur_camion m</div>
            <div>Portes : $portes_camion</div>
            <div>Poids : $poids_camion kg</div>
        </div>
        EOT;
}

function affiche2Roues(){


    $couleurs = ["red" => "#a33131", "green" => "#5DA331", "blue" => "#2955A3", "black" => "#000000"];
    if(!isset($_SESSION["2roues"]) || isset($_GET["reset_2r"])){
        $_SESSION["2roues"] = new Deux_roues("blue", 110);
    }

    if(isset($_GET["loaded_5"])){
        $_SESSION["2roues"] = new Deux_roues($_GET["couleur_2r"], $_GET["poids_2r"]);
        $_SESSION["2roues"]->mettre_essence($_GET["essence_2r"]);
        $_SESSION["2roues"]->ajouterPersonne($_GET["personnes_2r"]);
    }
    $couleur = $_SESSION["2roues"]->getCouleur();
    echo <<<EOT
            <style>
                .carrosserie-2r {
                    background-color: $couleurs[$couleur];
                }
            </style>
         EOT;
    $poids_2roues = $_SESSION["2roues"]->getPoids();


    echo <<<EOT
        <div class="deux_roues_container">
            <div class="deux_roues">
            
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case carrosserie_grise arrondi_2r"></div>
            
                <div class="case carrosserie-2r arrondi_2r"></div>
                <div class="case carrosserie-2r"></div>
                <div class="case carrosserie-2r"></div>
                <div class="case carrosserie-2r"></div>
                <div class="case carrosserie_grise"></div>
                
                <div class="roue">
                    <div class="jante"></div>
                </div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="case case_vide"></div>
                <div class="roue">
                    <div class="jante"></div>
                </div>
            </div>
        </div>
        <div class="column">
            <div>Poids : $poids_2roues kg</div>
        </div>
        EOT;
}
?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Titre de la page</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
<body>
<div class="container-buttons">
    <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
    <div class="center-div"><b>Création de véhicules < Ex 3 < Section 2</b></div>
</div>
<div class="container-with-margin">
    <div class="row-container">
        <div class="column-container padding-left-and-right">
            <?php afficheVoiture(); ?>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <input type="hidden" name="rouler_v" value="true">
                    <input type="hidden" name="loaded_4" value="true">
                    <button type="submit" class="btn btn-primary">Faire rouler</button>
                </form>
            </div>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <div class="form-group">
                        <label for="repeindre_v">Repeindre</label>
                        <select name="repeindre_v" class="custom-select">
                            <option value="green" selected>Vert</option>
                            <option value="red">Rouge</option>
                            <option value="blue">Bleu</option>
                        </select>
                    </div>
                    <input type="hidden" name="loaded_1" value="true">
                    <button type="submit" class="btn btn-primary">Repeindre</button>
                </form>
            </div>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <div class="form-group">
                        <label for="personne_v">Ajouter personnes</label>
                        <input type="text" placeholder="Poids" name="personne_v" class="form-control">
                    </div>
                    <input type="hidden" name="loaded_2" value="true">
                    <button type="submit" class="btn btn-primary">Ajouter</button>
                </form>
            </div>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <input type="hidden" name="couleur_v" value="green">
                    <input type="hidden" name="nb_personne_v" value="2">
                    <input type="hidden" name="personne_v" value="65">
                    <input type="hidden" name="poids_v" value="1400">
                    <input type="hidden" name="loaded_3" value="true">
                    <button type="submit" class="btn btn-primary">Créer une voiture verte de 1400 kg et ajouter 2 passagers de 65 kg</button>
                </form>
            </div>
        </div>
        <div class="vertical-separator z-index-2"></div>
        <div class="column-container padding-left-and-right z-index-2 white-background">
            <?php afficheCamion(); ?>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <input type="hidden" name="couleur_c" value="green">
                    <input type="hidden" name="longueur_c" value="12">
                    <input type="hidden" name="portes_c" value="4">
                    <input type="hidden" name="poids_c" value="11500">
                    <input type="hidden" name="remorque_c" value="7">
                    <input type="hidden" name="loaded_4" value="true">
                    <button type="submit" class="btn btn-primary">Créer un camion de 11500 kg, d'une longueur de 12 m, avec 4 portes, avec une remorque de 7 mètres</button>
                </form>
            </div>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <input type="hidden" name="reset_c" value="true">
                    <button type="submit" class="btn btn-primary">Reset camion</button>
                </form>
            </div>
            </br>
            <?php affiche2Roues(); ?>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <input type="hidden" name="couleur_2r" value="black">
                    <input type="hidden" name="poids_2r" value="110">
                    <input type="hidden" name="personnes_2r" value="85">
                    <input type="hidden" name="essence_2r" value="15">
                    <input type="hidden" name="loaded_5" value="true">
                    <button type="submit" class="btn btn-primary">Créer un Deux roues noir de 110 Kg, avec une personne de 85 Kg et lui mettre 15 litres d'essence</button>
                </form>
            </div>
            <div class="container-with-margin">
                <form method="GET" action="serie_2_exo_3.php">
                    <input type="hidden" name="reset_2r" value="true">
                    <button type="submit" class="btn btn-primary">Reset 2 roues</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
