<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calcul du capital</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
<body>
<?php
    function calculeCapital($capitalInit, $tauxAnnuel, $nbAnnees){
        $capitalFinal = $capitalInit*($tauxAnnuel + 1);
        for($i = 2 ; $i <= $nbAnnees ; $i++){
            $capitalFinal *= ($tauxAnnuel + 1);
        }
        return round($capitalFinal, 2);
    }
    function nbAnneesDoublerCapital($capital, $tauxAnnuel){
        $nbAnneesDoublerCapital = 0;
        $capitalCourant = calculeCapital($capital, $tauxAnnuel, $nbAnneesDoublerCapital);
        while ($capitalCourant <= $capital * 2){
            $nbAnneesDoublerCapital ++;
            $capitalCourant = calculeCapital($capital, $tauxAnnuel, $nbAnneesDoublerCapital);
        }
        return $nbAnneesDoublerCapital;
    }
    function capitalPour10kApres25ans($tauxAnnuel){
        $nbAnnees = 25;
        $seuilCapital = 10000;
        $capitalCpt = 0;
        $capital = calculeCapital($capitalCpt, $tauxAnnuel, $nbAnnees);
        while ($capital <= $seuilCapital){
            $capitalCpt++;
            $capital = calculeCapital($capitalCpt, $tauxAnnuel, $nbAnnees);
        }
        return $capitalCpt;
    }
?>
<div class="container-buttons">
    <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
    <div class="center-div"><b>Calcul du capital < Ex 3 < Section 1</b></div>
</div>
<div class="container-with-margin">
    <div><b>Valeur du capital après N années</b></div>
    </br>
    <form method="GET" action="serie_1_exo_4.php">
        <div class="form-group">
            <label for="capital_1">Capital</label>
            <input type="number" class="form-control" name="capital_1" placeholder="Capital">
        </div>

        <div class="form-group">
            <label for="tauxAnnuel_1">Taux annuel</label>
            <input type="number" class="form-control" name="tauxAnnuel_1" placeholder="Taux Annuel" step="0.0000001">
        </div>
        <div class="form-group">
            <label for="nbAnnees_1">Nombre d'années</label>
            <input type="number" class="form-control" name="nbAnnees_1" placeholder="Nombre d'années">
        </div>
        <input type="hidden" name="loaded_1" value="true">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </br>
    <?php
        $capital = $_GET["capital_1"];
        $tauxAnnuel = $_GET["tauxAnnuel_1"];
        $nbAnnees = $_GET["nbAnnees_1"];

        if(($capital == "" || $tauxAnnuel == "" || $nbAnnees == "") && $_GET["loaded_1"] == "true"){
            echo "Données incomplètes";
        }
        elseif(isset($capital, $tauxAnnuel, $nbAnnees)){
            echo "Capital final après $nbAnnees " . (($nbAnnees == 1) ? "an : " : "ans : ") . calculeCapital($capital, $tauxAnnuel, $nbAnnees) . "€";
        }
    ?>
</div>
<div class="container-with-margin">
    <div><b>Nombre d'années pour doubler son capital</b></div>
    </br>
    <form method="GET" action="serie_1_exo_4.php">
        <div class="form-group">
            <label for="capital_2">Capital</label>
            <input type="number" class="form-control" name="capital_2" placeholder="Capital">
        </div>

        <div class="form-group">
            <label for="tauxAnnuel_2">Taux annuel</label>
            <input type="number" class="form-control" name="tauxAnnuel_2" placeholder="Taux Annuel" step="0.0000001">
        </div>
        <input type="hidden" name="loaded_2" value="true">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </br>
    <?php
    $capital = $_GET["capital_2"];
    $tauxAnnuel = $_GET["tauxAnnuel_2"];

    if(($capital == "" || $tauxAnnuel == "") && $_GET["loaded_2"] == "true"){
        echo "Données incomplètes";
    }
    elseif(isset($capital, $tauxAnnuel)){
        $nbAnneesDoublerCapital = nbAnneesDoublerCapital($capital, $tauxAnnuel);
        $capitalApresBoublage = calculeCapital($capital, $tauxAnnuel, $nbAnneesDoublerCapital);
        echo "Pour doubler un capital de $capital € en ayant un taux annuel de $tauxAnnuel, il faudra ". $nbAnneesDoublerCapital ." an(s) : Le capital sera alors à $capitalApresBoublage €";
    }
    ?>
</div>
<div class="container-with-margin">
    <div><b>Capital qu'il faut placer pour qu'il dépasse 10 000 € après 25 ans</b></div>
    </br>
    <form method="GET" action="serie_1_exo_4.php">
        <div class="form-group">
            <label for="tauxAnnuel_3">Taux annuel</label>
            <input type="number" class="form-control" name="tauxAnnuel_3" placeholder="Taux Annuel" step="0.0000001">
        </div>
        <input type="hidden" name="loaded_3" value="true">
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    </br>
    <?php
    $tauxAnnuel = $_GET["tauxAnnuel_3"];

    if($tauxAnnuel == "" && $_GET["loaded_3"] == "true"){
        echo "Données incomplètes";
    }
    elseif(isset($tauxAnnuel)){
        $capitalAPlacer = capitalPour10kApres25ans($tauxAnnuel);
        echo "Il faut placer une capital de $capitalAPlacer € à un taux de $tauxAnnuel pour dépasser 10000 € après 25 ans";
    }
    ?>
</div>
</body>
</html>
