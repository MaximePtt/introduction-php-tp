<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Nombres parfaits entre 1 et 100</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
    <body>
        <div class="container-buttons">
            <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
            <div class="center-div"><b>Nombres parfaits entre 1 et 100 < Ex 1 < Section 1</b></div>
        </div>
        <div class="container-with-margin">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Nombre entier</th>
                    <th scope="col">Parfait</th>
                </tr>
                </thead>
                <tbody>
                    <?php
                    for($i = 1 ; $i <= 100 ; $i++) {
                        if(estParfait($i)) {
                            echo "<tr>";
                            echo "<td>$i</td>";
                            echo '<td>'."oui".'</td>';
                            echo "</tr>";
                        }
                    }

                    function estParfait($n){
                        $sommeDivPropres = 0;
                        for($i = 1 ; $i < $n ; $i++)
                            if($n % $i == 0)
                                $sommeDivPropres += $i;
                        return ($sommeDivPropres == $n) ? true : false;
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </body>
</html>
