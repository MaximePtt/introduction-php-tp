<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Calcul du prix de la place de cinéma</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
<body>
    <div class="container-buttons">
        <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
        <div class="center-div"><b>Calcul du prix de la place de cinéma < Ex 2 < Section 1</b></div>
    </div>
    <div class="container-with-margin">
        <form method="GET" action="serie_1_exo_2.php">
            <div class="form-group">
                <label for="inputJour">Jour</label>
                <select name="inputJour" class="custom-select">
                    <option value="" selected>--Choisissez un jour--</option>
                    <option value="1">Lundi</option>
                    <option value="2">Mardi</option>
                    <option value="3">Mercredi</option>
                    <option value="4">Jeudi</option>
                    <option value="5">Vendredi</option>
                    <option value="6">Samedi</option>
                    <option value="7">Dimanche</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputAge">Âge</label>
                <input type="number" class="form-control" name="inputAge" placeholder="Âge">
            </div>
            <div class="form-group form-check">
                <input type="checkbox" class="form-check-input" name="isEtudiantCheckBox">
                <label class="form-check-label" for="isEtudiantCheckBox">Etudiant</label>
            </div>
            <input type="hidden" name="loaded" value="true">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        </br>
        <?php
        function getPrixPlace($jour, $age, $etudiant){
            $tarif_reduit = 4.50;
            $tarif_plein = 7.00;
            if($etudiant || $jour == 3 || (($age < 18 || $age > 65) && ($jour < 6))) return $tarif_reduit;
            else return $tarif_plein;
        }

        if(($_GET["inputJour"] == "" || $_GET["inputAge"] == "") && $_GET["loaded"] == "true"){
            echo "Données incomplètes";
        }
        elseif(isset($_GET["inputJour"], $_GET["inputAge"])){
            echo "Prix place : " . getPrixPlace($_GET["inputJour"], $_GET["inputAge"], $_GET["isEtudiantCheckBox"]) . "€";
        }

        ?>
    </div>
</body>
</html>
