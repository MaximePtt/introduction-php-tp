<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Script de génération de tableau</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
    <link href="../style.css" rel="stylesheet">
</head>
    <body>
        <?php
            function echoTable($table){
                echo "<table class=\"table table-bordered\">";
                echo "<thead><tr>";
                echo '<th scope="col">Clé</th>';
                echo '<th scope="col" colspan="2"><div class="centered-text">Valeur</div></th>';
                echo "</tr></thead>";
                echo "<tbody>";
                foreach($table as $key => $value){
                    echo "<tr>";
                    echo '<td rowspan="' . (count($table[$key]) + 2) . '">' . $key . '</td>';
                    echo "<tr>";
                    echo '<th>Clé</th>';
                    echo '<th>Valeur</th>';
                    echo '</tr>';
                    foreach($table[$key] as $key2 => $value2){
                        echo "<tr>";
                        echo '<th>' . $key2 . '</th>';
                        echo '<td>' . $value2 . '</td>';
                        echo '</tr>';
                    }
                    echo '</tr>';
                }
                echo "</tbody>";
                echo "</table>";
            }
        ?>
        <div class="container-buttons">
            <a href="../index.php"><button type="button" class="btn btn-primary"><i class="fas fa-home"></i> Accueil</button></a>
            <div class="center-div"><b>Script de génération de tableau < Ex 4 < Section 1</b></div>
        </div>
        <div class="container-with-margin">

            <?php
                $tab_dupont = ["prénom" => "PAUL","profession" => "ministre","age" => 50];
                $tab_durand = ["prénom" => "ROBERT","profession" => "agriculteur","age" => 45];
                $tab = ["Dupont" => $tab_dupont, "Durant" => $tab_durand];
                echoTable($tab);
            ?>
        </div>
    </body>
</html>
